FROM python:3.7
MAINTAINER Alberto Paparelli <alberto@paparelli.com.ar>

#ENV PYTHONUNBUFFERED 1

RUN mkdir -p /usr/src/app


WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

CMD ["gunicorn", "--config", "gunicorn_config.py", "app:app", "--reload"]

EXPOSE 8001
