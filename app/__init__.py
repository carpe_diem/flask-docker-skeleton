import logging
import os
import unittest
import sys

from flask import Config, Flask

from flask_migrate import Migrate

from app.api.schemas import register_jsonapi
from app.models.init_db import db


#logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
#logger = logging.getLogger(__name__)

import datetime
from flask import Flask, jsonify, Response
from flask.json import JSONEncoder
from flask_cors import CORS



class BaseJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, datetime.date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


class BaseResponse(Response):
    default_mimetype = 'application/json'

    @classmethod
    def force_type(cls, rv, environ=None):
        if isinstance(rv, dict):
            rv = jsonify(rv)
        return super(BaseResponse, cls).force_type(rv, environ)




class BaseFlask(Flask):
    response_class = BaseResponse
    json_encoder = BaseJSONEncoder  # encoder to handle date as ISO8601 format


    def __init__(
            self,
            import_name
    ):
        Flask.__init__(
            self,
            import_name
        )
        # set config
        app_settings = os.getenv('APP_SETTINGS')
        self.config.from_object(app_settings)

        # configure gunicorn logging if running on it
        gunicorn_logger = logging.getLogger('gunicorn.error')
        if gunicorn_logger.handlers:
            self.logger.handlers = gunicorn_logger.handlers
            # use gunicorn_logger.level in case we wanna honor --log-level
            gunicorn_logger.setLevel(self.config['LOGGING_LEVEL'])

        flask_app_logger = logging.getLogger('flask.app')
        flask_app_logger.setLevel(self.config['LOGGING_LEVEL'])

        services_logger = logging.getLogger("services")
        services_logger.setLevel(self.config['LOGGING_LEVEL'])


        # enable CORS
        #CORS(self)

    def __logging_handler(self):
        handler = None
        if self.config['LOGGING_LOCATION'] == 'STDOUT':
            handler = logging.StreamHandler(sys.stdout)
        else:
            handler = logging.FileHandler(self.config['LOGGING_LOCATION'])
        handler.setLevel(self.config['LOGGING_LEVEL'])
        handler.setFormatter(logging.Formatter(self.config['LOGGING_FORMAT']))
        return handler

def create_app():

    #app = Flask(__name__, instance_relative_config=True)
    app = BaseFlask(__name__)


    config = Config(root_path=os.path.dirname(os.path.realpath(__file__)))
    config.from_object(os.getenv('APP_SETTINGS'))

    gunicorn_logger = logging.getLogger('gunicorn.error')
    if gunicorn_logger.handlers:
        app.logger.handlers = gunicorn_logger.handlers
        gunicorn_logger.setLevel(config['LOGGING_LEVEL'])

    app.logger.setLevel(logging.DEBUG)
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)


    migrate = Migrate()
    migrate.init_app(app, db)

    db.init_app(app)
    register_jsonapi(app)

    return app


app = create_app()


# Flask Commands
@app.cli.command('test')
def test_command():
    """Run unit tests."""
    # TODO Args option for coverage and verbosity
    tests = unittest.TestLoader().discover('tests', pattern='test_*.py')
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command('recreatedb')
def recreatedb_command():
    """Recreate db."""
    db.drop_all()
    db.create_all()
    db.session.commit()

#if __name__ == '__name__':
#    app.run()
