"""Base Unit Test Case"""
import unittest

from app import app
from app.models.init_db import db


class BaseTestCase(unittest.TestCase):
    """A base class for test setup"""

    def create_app(self):
        """ """
        app.config.from_object('app.config.TestingConfig')
        return app

    def setUp(self):
        """ setup method for unittest, called before each test function"""
        app = self.create_app()
        self.client = app.test_client(self)
        self.context = app.app_context()
        self.request_context = app.test_request_context()
        db.create_all()
        db.session.commit()

    def tearDown(self):
        """Called after each test function"""
        db.session.remove()
        db.drop_all()
