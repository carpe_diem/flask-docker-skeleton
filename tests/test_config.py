# -*- coding: utf-8 -*-
""" Tests for all flask configurations in app/config.py.
This file shouldn't really be modified, unless you add
new configurations.
"""
import os

from flask import current_app

from app import app
from tests.base import BaseTestCase


class TestTestingConfig(BaseTestCase):

    def test_app_is_testing(self):
        self.assertTrue(app.config['SECRET_KEY'] == os.getenv('SECRET_KEY'))
        self.assertTrue(app.config['DEBUG'])
        self.assertTrue(app.config['TESTING'])
        self.assertFalse(app.config['PRESERVE_CONTEXT_ON_EXCEPTION'])
        self.assertTrue(app.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get('DATABASE_TEST_URL'))


class TestDevelopmentConfig(BaseTestCase):

    def create_app(self):
        """ Overwrite create_app with config Developer Config. """
        app.config.from_object('app.config.DevelopmentConfig')
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config['SECRET_KEY'] == os.getenv('SECRET_KEY'))
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app is None)
        self.assertTrue(app.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get('DATABASE_URL'))


class TestProductionConfig(BaseTestCase):

    def create_app(self):
        """ Overwrite create_app with config Production Config. """
        app.config.from_object('app.config.ProductionConfig')
        return app

    def test_app_is_production(self):
        self.assertTrue(app.config['SECRET_KEY'] == os.getenv('SECRET_KEY'))
        self.assertFalse(app.config['DEBUG'])
        self.assertFalse(app.config['TESTING'])

