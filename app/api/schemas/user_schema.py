
from flask_rest_jsonapi import ResourceList, ResourceRelationship

from marshmallow_jsonapi.flask import Schema, Relationship
from marshmallow_jsonapi import fields

from app.models.init_db import db
from app.models.user import User


class UserSchema(Schema):
    class Meta:
        type_ = 'user'
        self_view = 'user_detail'
        self_view_kwargs = {'id': '<id>'}
        self_view_many = 'user_list'

    id = fields.Integer(as_string=True, dump_only=True)
    last_name = fields.Str(required=True, load_only=True)
    first_name = fields.Str(required=True, load_only=True)
    display_name = fields.Function(lambda obj: "{} <{}>".format(obj.name.upper(), obj.email))
    email = fields.Email(load_only=True)
    created = fields.Date()

    #roles = Relationship(self_view='user_roles',
    #                         self_view_kwargs={'id': '<id>'},
    #                         related_view='role_list',
    #                         related_view_kwargs={'id': '<id>'},
    #                         many=True,
    #                         schema='RoleSchema',
    #                         type_='role')


class UserList(ResourceList):
    schema = UserSchema
    data_layer = {'session': db.session,
                  'model': User}
