# -*- coding: utf-8 -*-
""" Base models """
from datetime import datetime

from sqlalchemy.ext.declarative import declarative_base

from app.models.init_db import db


Base = declarative_base()


class BaseModel(Base):
    """Base data model for all objects"""
    __abstract__ = True

    def __init__(self, *args):
        super().__init__(*args)

    def __repr__(self):
        """Define a way to print models"""
        return '{}({})'.format(self.__class__.__name__, {
                               column: value
                               for column, value in self.as_dict().items()})


class TimeStampModel(BaseModel):
    __abstract__ = True    

    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow,
                        onupdate=datetime.utcnow)
