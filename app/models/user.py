# -*- coding: utf-8 -*-
""" User models """

from sqlalchemy.sql import func

from app.models.init_db import db
from app.models.base import TimeStampModel


class User(TimeStampModel):
    """ Model for Users """
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False)
    email_verified = db.Column(db.Boolean, default=False, nullable=False)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    #created_date = db.Column(db.DateTime, nullable=False, default=func.now())
    active = db.Column(db.Boolean, default=False, nullable=False)

    roles = db.relationship('Role', secondary='user_roles')


class Role(TimeStampModel):
    """ Model for Roles """
    __tablename__ = "roles"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String())
    active = db.Column(db.Boolean, default=True, nullable=False)


class UserRoles(TimeStampModel):
    """ Define the UserRoles association model. """
    __tablename__ = "user_roles"

    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.id', ondelete='CASCADE'),
                        primary_key=True)
    role_id = db.Column(db.Integer,
                        db.ForeignKey('roles.id', ondelete='CASCADE'),
                        primary_key=True)
