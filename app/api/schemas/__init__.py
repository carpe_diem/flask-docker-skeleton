
from flask import Flask

from flask_rest_jsonapi import Api

from app.api.schemas.user_schema import UserList


def register_jsonapi(app: Flask):
    """flask-rest-jsonapi endpoints."""
    api = Api(app)

    user_endpoints(api)


def user_endpoints(api: Api):
    """Endpoints related to users."""
    api.route(UserList, 'user_list', '/user')
